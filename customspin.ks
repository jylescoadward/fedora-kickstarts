# https://docs.fedoraproject.org/en-US/fedora/f35/install-guide/appendixes/Kickstart_Syntax_Reference/

%include fedora-live-base.ks

# Configure installation method
url --mirrorlist="https://mirrors.fedoraproject.org/metalink?repo=fedora-35&arch=x86_64"
repo --name=fedora-updates --mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=updates-released-f35&arch=x86_64" --cost=0
repo --name=rpmfusion-free --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-35&arch=x86_64" --includepkgs=rpmfusion-free-release
repo --name=rpmfusion-free-updates --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=free-fedora-updates-released-35&arch=x86_64" --cost=0
repo --name=rpmfusion-nonfree --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-35&arch=x86_64" --includepkgs=rpmfusion-nonfree-release
repo --name=rpmfusion-nonfree-updates --mirrorlist="https://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-fedora-updates-released-35&arch=x86_64" --cost=0

# Configure Boot Loader
bootloader --driveorder=sda

# Remove all existing partitions
clearpart --drives=sda --all

# zerombr
zerombr

#Create required partitions (BIOS boot partition and /boot)
reqpart --add-boot

# Create Physical Partition
part swap --size=8192 --ondrive=sda
part / --size=16384 --grow --asprimary --ondrive=sda --fstype=xfs

# Configure Firewall
firewall --enabled

# Configure Network Interfaces
network --onboot=yes --bootproto=dhcp --hostname=workstation

# Configure Keyboard Layouts
keyboard us

# Configure Language During Installation
lang en_AU

# Services to enable/disable
services --disabled=mlocate-updatedb,mlocate-updatedb.timer,geoclue,avahi-daemon

# Configure Time Zone
timezone Australia/Perth

# Configure X Window System
xconfig --startxonboot

# Set Root Password
rootpw --lock

# Create User Account
# password is password, change it on first login pls ;w;
user --name=jyles --password=$2b$10$s477pCl/q6LjmSrr/NikR.DMYkRiVomJdxHevI7vosqwPqEvkXbRq --iscrypted --groups=wheel,sudo

# Package Selection
%packages
-openssh-server
-gssproxy
-nfs-utils
-sssd*
-abrt*
kernel
kernel-modules
kernel-modules-extra
@x86-baremetal-tools
anaconda
anaconda-install-env-deps
anaconda-live
@anaconda-tools
-fcoe-utils
-device-mapper-multipath
aajohan-comfortaa-fonts
glibc-all-langpacks
initscripts
chkconfig
@Base
@core
@standard
@hardware-support
@base-x
@firefox
@fonts
@xfce-office
@multimedia
@networkmanager-submodules
@printing
@^xfce-desktop-environment
@xfce-apps
@xfce-extra-plugins
@development-tools
syslinux
NetworkManager-openvpn-gnome
gimp
restic
nmap
tcpdump
ansible
thunderbird
vlc
calc
gstreamer-plugins-ugly
gstreamer1-plugins-ugly
redhat-rpm-config
rpmconf
strace
wireshark
ffmpeg
system-config-printer
git-review
gcc-c++
readline-devel
libX11-devel
libXt-devel
zlib-devel
bzip2-devel
xz-devel
pcre2-devel
libcurl-devel
python3
python3-virtualenvwrapper
python3-devel
nodejs
golang
libffi-devel
sqlite
exfat-utils
fuse-exfat
jq
icedtea-web
argon2
xournal
pykickstart
evince
firejail
ShellCheck
geteltorito
genisoimage
openssl
konsole
flatpak
flatpak-builder
flatpak-session-helper
python3-numpy
whois
xterm
thunderbird
nmap
wireshark
wget
lightdm-gtk-greeter-settings
gnome-font-viewer
cargo
blender
okular
micro
git-cola
lollypop
geany
xfce4-panel-profiles
dracut-live
%end

# Post-installation Script
%post --log=/root/ks-post.log

# Install dotfiles
    wget https://github.com/jylescoad-ward/dotfiles/archive/refs/heads/main.zip -O dotfiles.zip
    unzip dotfiles.zip -d dotfiles
    xfce4-panel-profiles load ./dotfiles/dotfiles-master/xfce4/panelprofile.tar.bz2

    # Set Backgrounds
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace0/last-image --set /usr/share/backgrounds/desktop-custom.png
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace0/image-style --set 2
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace1/last-image --set /usr/share/backgrounds/desktop-custom.png
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace1/image-style --set 2
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace2/last-image --set /usr/share/backgrounds/desktop-custom.png
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace2/image-style --set 2
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace3/last-image --set /usr/share/backgrounds/desktop-custom.png
    xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitorVirtual-1/workspace3/image-style --set 2

    # Set Theme and Icons
    xfconf-query --channel xsettings --property /Net/ThemeName --set PLATINUM
    xfconf-query --channel xsettings --property /Net/IconThemeName --set Haiku

    # Install everything else custom
    cp ./dotfiles/dotfiles-master/linux/* /


    # Make our theme default/global
    mkdir -p /etc/skel/.config/
    ln -s ~/.config/xfce4 /etc/skel/.config/xfce4

# Enable and Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

%end

# Reboot After Installation
reboot --eject
